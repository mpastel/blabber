This is proof of concept of usage: Rails, ActionCable & React
based on the article [How to Build a Twitter Clone with Rails, ActionCable and React](https://dev.to/rob__race/how-to-build-a-twitter-clone-with-rails-actioncable-and-react-721) with small improvements. But it's also clone of the original app [Build a twitter clone in 10 mins with Rails, CableReady, and StimulusReflex](https://github.com/hopsoft/chatter)

For building used docker templates from [Ruby On Whales](https://evilmartians.com/chronicles/ruby-on-whales-docker-for-ruby-rails-development)


Run the following commands to prepare your Docker dev env:
```
$ docker-compose build
$ docker-compose run runner yarn install
$ docker-compose run runner ./bin/setup
```
It builds the Docker image, installs Ruby and NodeJS dependencies, creates database, run migrations and seeds.