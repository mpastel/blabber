import React from 'react';
import Posts from "./posts";
import { ActionCableProvider } from "use-action-cable";

const PostsWrapper = (props) => {
    return (
        <ActionCableProvider url="/cable">
            <Posts {...props} />
        </ActionCableProvider>
    );
}

export default PostsWrapper;