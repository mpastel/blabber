Rails.application.routes.draw do
  resources :posts, only: [:index, :create] do
    get "like"
    get "repost"
  end

  root to: "posts#index"
end
